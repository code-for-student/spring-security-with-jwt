//package com.example.btb_spring_security_01_class.securityConfig;
//
//import com.example.btb_spring_security_01_class.service.UserServiceImp;
//import lombok.AllArgsConstructor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpStatus;
//import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.web.SecurityFilterChain;
//
//@Configuration
//@AllArgsConstructor
//public class DatabaseSecurityConfig {
//
//    private final UserServiceImp userServiceImp;
//    private final BCryptPasswordEncoder passwordEncoder;
//
//    @Bean
//    DaoAuthenticationProvider authenticationProvider(){
//        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
//        authProvider.setUserDetailsService(userServiceImp);
//        authProvider.setPasswordEncoder(passwordEncoder);
//        return authProvider;
//    }
//
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity)throws Exception{
//        httpSecurity.csrf().disable().authorizeHttpRequests(
//                (req) -> req.requestMatchers("/api/v1/users/user").hasRole("USER")
//                        .requestMatchers("/api/v1/users/admin").hasRole("ADMIN")
//                        .requestMatchers("/api/v1/users/user_admin").hasAnyRole("USER","ADMIN")
//                        .requestMatchers("/","/v3/api-docs/**","/swagger-ui/**",
//                                "/swagger-ui.html","/api/v1/users/home").permitAll()
//                        .anyRequest()
//                        .authenticated()
//        ).formLogin().disable().httpBasic().authenticationEntryPoint((request, response, authException) -> {
//            response.sendError(HttpStatus.UNAUTHORIZED.value(),HttpStatus.UNAUTHORIZED.getReasonPhrase());
//        });
//
//
//
//        return httpSecurity.build();
//    }
//
//
//
//
//
//
//}
