package com.example.btb_spring_security_01_class.repository;

import com.example.btb_spring_security_01_class.model.UserInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserRepository {

//    @Select("SELECT * FROM users WHERE name = #{name}")
//    UserInfo getUserByName(@Param("name") String name);


    @Select("""
            SELECT rt.role_name FROM user_role_tb urt
            INNER JOIN role_tb rt ON rt.id = urt.role_id
            WHERE urt.user_id = #{userId}
            
            """)
    List<String> getRoleByUserId(@Param("userId") Integer userId);

    @Select("SELECT * FROM user_tb WHERE email = #{email} OR username = #{email}")
    @Results(id = "mappUser", value = {
            @Result(property = "userId", column = "id"),
            @Result(property = "userName", column = "username"),
            @Result(property = "email", column = "email"),
            @Result(property = "password", column = "password"),
            @Result(property = "roles",column = "id",
                    many = @Many(select = "getRoleByUserId"))
    })
    UserInfo findByEmail(@Param("email") String email);

//    @Select("INSERT INTO user_tb(username, email, password) " +
//            "VALUES (#{request.name}, #{request.email},#{request.password}) RETURNING id")
//    Integer addNewUser(@Param("request")RegisterRequest registerRequest);
//
//    @Select("INSERT INTO user_role_tb (user_id, role_id) " +
//            "VALUES (#{userId}, #{roleId})")
//    Integer addRoleUserByUserId(@Param("userId") Integer userId, @Param("roleId") Integer roleId);




}
