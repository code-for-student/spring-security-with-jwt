package com.example.btb_spring_security_01_class.service;

import com.example.btb_spring_security_01_class.model.AuthenticationRequest;
import com.example.btb_spring_security_01_class.model.AuthenticationResponse;
import com.example.btb_spring_security_01_class.repository.UserRepository;
import com.example.btb_spring_security_01_class.securityConfig.JwtAuthenticationFilter;
import com.example.btb_spring_security_01_class.securityConfig.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.apache.ibatis.annotations.Select;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final JwtUtil jwtUtil;
    private final UserServiceImp userServiceImp;
    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    public AuthenticationResponse authenticate(AuthenticationRequest request){
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = userRepository.findByEmail(request.getEmail());

        System.out.println(user);

        /***
         * It use for generate token to authenticate
         */

        var jwtToken = jwtUtil.generateToken(user);
        return AuthenticationResponse.builder().token(jwtToken).build();
    }


}
