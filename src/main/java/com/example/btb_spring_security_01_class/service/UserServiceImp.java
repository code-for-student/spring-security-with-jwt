package com.example.btb_spring_security_01_class.service;

import com.example.btb_spring_security_01_class.model.UserInfo;
import com.example.btb_spring_security_01_class.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImp implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserInfo userInfo = userRepository.findByEmail(username);
        if (userInfo == null){
            throw new UsernameNotFoundException("user not found");
        }
        System.out.println(userInfo);
        return userInfo;
    }
}
